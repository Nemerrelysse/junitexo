package fr.eql.autom.school;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TeacherTest {

    Teacher teacher;

    @Before
    public void setUp() {
        teacher=new Teacher("Sophie","Depierre");
    }

    @Test
    public void test(){
        double wage=1645.26;
        int abs=2;
        teacher.setWage(wage);
        teacher.setNbAbsenceCurrentMonth(abs);
        assertEquals("Le salaire ne correspond pas.", wage,teacher.getWage(),0.0001);
        assertEquals("Le nombre d'absences ne correspond pas",
                abs,teacher.getNbAbsenceCurrentMonth());
        teacher.introduce();
    }



    @After
    public void tearDown()  {
        teacher=null;
    }
}