package fr.eql.autom.school;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AdminStaffTest {

    AdminStaff admin;

    @Before
    public void setUp() {
        admin=new AdminStaff("Sophie","Depierre");
    }

    @Test
    public void test(){
        double wage=2536.25;
        int abs=3;
        admin.setWage(wage);
        admin.setNbAbsenceCurrentMonth(abs);
        assertEquals("Le salaire ne correspond pas.", wage,admin.getWage(),0.0001);
        assertEquals("Le nombre d'absences ne correspond pas",
                abs,admin.getNbAbsenceCurrentMonth());
        admin.introduce();
    }



    @After
    public void tearDown()  {
        admin=null;
    }
}