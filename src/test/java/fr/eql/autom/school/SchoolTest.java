package fr.eql.autom.school;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

public class SchoolTest {
    static Logger logger= LoggerFactory.getLogger(SchoolTest.class);
    @Test
    public void test() throws IOException {
        Properties persons=new Properties();
        persons.load(new FileInputStream("src/main/resources/persons.properties"));

        //check that student are persons, and admin and teacher are school staff
        Person student=new Student(persons.getProperty("prenom1"), persons.getProperty("nom1"),9);
        assertNotNull(student);
        SchoolStaff admin=new AdminStaff(persons.getProperty("prenom2"),persons.getProperty("nom2"));
        assertNotNull(student);
        SchoolStaff teacher=new Teacher(persons.getProperty("prenom3"), persons.getProperty("nom3"));
        assertNotNull(student);
        logger.info("Heritage verified");

        //verify the setters and getters
        admin.setWage(1542.29);
        assertEquals("admin staff wage doesn't match",1542.29,admin.getWage(),0.0001);
        teacher.setWage(2352.12);
        assertEquals("teacher wage doesn't match",2352.12,teacher.getWage(),0.0001);
        admin.setNbAbsenceCurrentMonth(1);
        assertEquals("admin staff absences doesn't match",1,admin.getNbAbsenceCurrentMonth());
        teacher.setNbAbsenceCurrentMonth(3);
        assertEquals("teacher absences doesn't match",3,teacher.getNbAbsenceCurrentMonth());
        logger.info("Getters and Setters verified");

        //verify the riseWage method
        School school=new School();
        school.riseWage(admin,10.63);
        assertEquals("admin staff raised wage doesn't match",1552.92,admin.getWage(),0.0001);
        school.riseWage(teacher,155.02);
        assertEquals("teacher raised wage doesn't match",2507.14,teacher.getWage(),0.0001);
        logger.info("riseWage method verified");

        //verify the askForRise method
        admin.setNbAbsenceCurrentMonth(5);
        admin.askForRise(school,500);
        assertEquals("admin staff raised wage doesn't match",1552.92,admin.getWage(),0.0001);
        teacher.askForRise(school, 20);
        assertEquals("teacher raised wage doesn't match",2527.14,teacher.getWage(),0.0001);
        logger.info("askForRise method verified");

        //list of SchoolStaff
        List<SchoolStaff> staffList=new ArrayList<>();
        int n=3;
        for(int i=0;i<7;++i,++n){
            if (i%2==0){
                staffList.add(i,new Teacher(persons.getProperty("prenom"+n),persons.getProperty("nom"+n)));
            }else{
                staffList.add(i,new AdminStaff(persons.getProperty("prenom"+n),persons.getProperty("nom"+n)));
            }
            staffList.get(i).setWage(Math.random()*5000.0);
        }

        logger.info("SchoolStaff list display");
        for (SchoolStaff schoolStaff : staffList) {
            schoolStaff.introduce();
        }



    }
}
