package fr.eql.autom.school;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

public class StudentTest {
    static Logger logger= LoggerFactory.getLogger(School.class);
    Student student;
    Student student2;

    @Before
    public void setUp() {
        student=new Student("Sophie","Depierre",8);
    }

    @Test
    public void testAbsent(){
        student.setAbsent(true);
        logger.info("Student is absent : "+student.isAbsent());
        assertTrue(student.isAbsent());
    }

    @Test
    public void testLevel(){
        assertEquals("the student level doesn't match",Level.CE2,student.getLevel());
        student.setLevel(Level.CM1);
        assertEquals("the student level doesn't match",Level.CM1,student.getLevel());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAge(){
        student2=new Student("Margaux","Vigneron",11);
    }



    @After
    public void tearDown()  {
    }
}