package fr.eql.autom.school;

public enum Level {
    CP,
    CE1,
    CE2,
    CM1,
    CM2;
}
