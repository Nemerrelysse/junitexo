package fr.eql.autom.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class School {
    static Logger logger= LoggerFactory.getLogger(School.class);

    //rise up the wage of a member of the staff by adding the rise value to the current wage
    public void riseWage(SchoolStaff schoolStaff, double rise){
        schoolStaff.setWage(schoolStaff.getWage()+rise);
        logger.info("Wage raised");
    }

    //the level of the student is defined according to their age
    public static void chooseLevel(Student student){
        switch (student.getAge()){
            case 6 : student.setLevel(Level.CP);
                break;
            case 7 : student.setLevel(Level.CE1);
                break;
            case 8 : student.setLevel(Level.CE2);
                break;
            case 9 : student.setLevel(Level.CM1);
                break;
            case 10 : student.setLevel(Level.CM2);
                break;
            default:
                logger.warn("Eleve hors cursus, contactez l'administration");
        }
    }
}
