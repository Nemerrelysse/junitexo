package fr.eql.autom.school;

public interface SchoolStaff {
    //getters
    double getWage();
    int getNbAbsenceCurrentMonth();

    //setters
    void setWage(double wage);
    void setNbAbsenceCurrentMonth(int nbAbsenceCurrentMonth);

    //prints a presentation of the staff member
    void introduce();
    void askForRise(School school, double rise);
}
