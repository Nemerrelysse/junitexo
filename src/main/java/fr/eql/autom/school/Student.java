package fr.eql.autom.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Student extends Person{
    static Logger logger= LoggerFactory.getLogger(Student.class);
    private int age;
    private boolean absent;
    private Level level;
    public Student(String firstName, String lastName, int age) {
        super(firstName,lastName);
        //chesk if the age is between 6 and 10
        if(age > 5 && age < 11){
            this.age = age;
        }else {
            logger.error("Age out of bounds");
            throw new IllegalArgumentException("Age out of bounds");
        }
        //the school define the level depending on the age
        School.chooseLevel(this);
        logger.info("The school decided the level of the student");
        logger.info("Student created");
    }
    public void learn(){

    }

    //getters
    public boolean isAbsent() {
        return absent;
    }

    public int getAge(){
        return age;
    }
    public Level getLevel() {
        return level;
    }
    //setters
    public void setLevel(Level level) {
        this.level = level;
    }
    public void setAbsent(boolean absent) {
        this.absent = absent;
    }
}
