package fr.eql.autom.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Teacher extends Person implements SchoolStaff{
    static Logger logger= LoggerFactory.getLogger(Teacher.class);
    private double wage;
    private int nbAbsenceCurrentMonth;
    public Teacher(String firstName, String lastName) {
        super(firstName,lastName);
        logger.info("Teacher created");
    }
    public double getWage() {
        return wage;
    }
    public int getNbAbsenceCurrentMonth() {
        return nbAbsenceCurrentMonth;
    }

    //setters
    public void setWage(double wage) {
        this.wage = wage;
    }
    public void setNbAbsenceCurrentMonth(int nbAbsenceCurrentMonth) {
        this.nbAbsenceCurrentMonth = nbAbsenceCurrentMonth;
    }

    //prints a presentation of the teacher
    public void introduce(){
        logger.info("Je m'appelle "+firstName+" "+lastName+". Je suis prof et je gagne "+wage+"€.");
    }

    //ask the school for a rise : the wage is raised if there is less than 5 absences this month
    public void askForRise(School school, double rise) {
        if (nbAbsenceCurrentMonth<5){
            school.riseWage(this,rise);
        }else {
            logger.info("Rise denied, too much absences");
        }
    }
}
